use std::path::{PathBuf, MAIN_SEPARATOR};

use anyhow::Error;
use structopt::StructOpt;

mod days;
mod download;
mod run;

#[derive(StructOpt)]
/// Download inputs and problems for advent of code 2021.
struct AdventOfCodeCli {
    /// The day numbers to run
    days: Vec<u32>,
    /// The command to run
    #[structopt(subcommand)]
    command: Command,
}

#[derive(StructOpt)]
enum Command {
    /// Download the problem
    #[structopt(name = "download", aliases = &["dl", "d"])]
    Download {},
    #[structopt(name = "run", aliases = &["r"])]
    Run {
        part: Option<u32>,
        #[structopt(name = "debug", short = "d")]
        debug: bool,
        #[structopt(name = "manual", short = "m")]
        manual: bool,
        #[structopt(name = "example", short = "e")]
        example: bool,
        #[structopt(name = "input", short = "i")]
        input: Option<PathBuf>,
    },
    #[structopt(name = "test")]
    Test {},
    #[structopt(name = "bench")]
    Bench {
        #[structopt(name = "n", short = "n", default_value = "1000")]
        n: u32,
        #[structopt(name = "input", short = "i")]
        input: Option<PathBuf>,
    },
}

fn main() -> Result<(), Error> {
    let AdventOfCodeCli { mut days, command } = AdventOfCodeCli::from_args();
    if days.is_empty() {
        days.extend(days::list()?);
    }

    let mut output = Ok(());
    match command {
        Command::Download {} => {
            for day in days {
                output = output.and(download::main(day));
            }
        }
        Command::Run {
            part,
            debug,
            manual,
            example,
            input,
        } => {
            for day in days {
                let (input, out) = match (manual, example, input.clone()) {
                    (true, ..) => (None, false),
                    (_, _, Some(p)) => (Some(p), false),
                    (_, false, None) => (
                        Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into()),
                        true,
                    ),
                    (_, true, None) => (
                        Some(format!("inputs{}day{:02}_example.txt", MAIN_SEPARATOR, day).into()),
                        false,
                    ),
                };
                output = output.and(run::main(
                    day,
                    part,
                    debug,
                    false,
                    out,
                    None,
                    input.as_ref(),
                ));
            }
        }
        Command::Test {} => {
            for day in days {
                let input = Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into());
                output = output.and(run::main(
                    day,
                    None,
                    false,
                    true,
                    false,
                    None,
                    input.as_ref(),
                ));
            }
        }
        Command::Bench { n, mut input } => {
            for day in days {
                if input.is_none() {
                    input = Some(format!("inputs{}day{:02}.txt", MAIN_SEPARATOR, day).into())
                }
                output = output.and(run::main(
                    day,
                    None,
                    false,
                    true,
                    false,
                    Some(n),
                    input.as_ref(),
                ));
            }
        }
    };

    output
}
