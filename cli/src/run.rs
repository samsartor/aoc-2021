use anyhow::{bail, Context, Error};
use std::fs::File;
use std::path::PathBuf;
use std::process::{Command, Stdio};

pub fn main(
    day: u32,
    part: Option<u32>,
    debug: bool,
    test: bool,
    output: bool,
    bench: Option<u32>,
    input: Option<&PathBuf>,
) -> Result<(), Error> {
    let mut com = Command::new("cargo");
    com.arg("run");
    com.arg(format!("--bin=day{}", day));
    if debug {
        com.arg("--debug");
    } else {
        com.arg("--release");
    }
    com.arg("--");
    if let Some(part) = part {
        com.arg(format!("day{}::part{}", day, part));
    }
    if test {
        com.env("AOC_TESTING", "1");
    }
    if output {
        com.env("AOC_OUTPUT", "1");
    }
    if let Some(n) = bench {
        com.env("AOC_BENCH", n.to_string());
    }
    com.stdout(Stdio::inherit());
    com.stderr(Stdio::inherit());

    if let Some(path) = input {
        com.stdin(File::open(path).with_context(|| format!("could not open {:?}", path))?);
    } else {
        com.stdin(Stdio::inherit());
    }

    let status = com.status()?;
    if !status.success() {
        if test {
            bail!("test failed")
        } else {
            bail!("could not run day {}", day)
        }
    }

    Ok(())
}
