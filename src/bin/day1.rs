#![feature(array_windows)]

use aoc::*;

fn part1(input: Vec<usize>) -> usize {
    input.array_windows().filter(|[a, b]| b > a).count()
}

fn part2(input: Vec<usize>) -> usize {
    let summed: Vec<_> = input.array_windows().map(|[a, b, c]| a + b + c).collect();
    summed.array_windows().filter(|[a, b]| b > a).count()
}

fn main() {
    run(part1);
    run(part2);
}
