use aoc::*;

fn part1(i: Vec<Line<&str>>) -> usize {
    let mut total = 0;
    for line in i {
        let mut stack = Vec::new();
        for c in line.inner.chars() {
            let (expected, points) = match c {
                '(' | '[' | '{' | '<' => {
                    stack.push(c);
                    continue;
                }
                ')' => ('(', 3),
                ']' => ('[', 57),
                '}' => ('{', 1197),
                '>' => ('<', 25137),
                _ => panic!(),
            };
            if Some(expected) != stack.pop() {
                total += points;
            }
        }
    }
    total
}

fn part2(i: Vec<Line<&str>>) -> usize {
    let mut scores = Vec::new();
    'lines: for line in i {
        let mut stack = Vec::new();
        for c in line.inner.chars() {
            let expected = match c {
                '(' | '[' | '{' | '<' => {
                    stack.push(c);
                    continue;
                }
                ')' => '(',
                ']' => '[',
                '}' => '{',
                '>' => '<',
                _ => panic!(),
            };
            if Some(expected) != stack.pop() {
                continue 'lines;
            }
        }
        scores.push(
            stack
                .into_iter()
                .rev()
                .map(|c| match c {
                    '(' => 1,
                    '[' => 2,
                    '{' => 3,
                    '<' => 4,
                    _ => panic!(),
                })
                .fold(0, |a, b| a * 5 + b),
        );
    }
    scores.sort();
    scores[scores.len() / 2]
}

fn main() {
    run(part1);
    run(part2);
}
