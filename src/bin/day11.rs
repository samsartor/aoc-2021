use aoc::*;

fn step(octo: &mut Grid<u32>) -> usize {
    for e in octo.iter_mut() {
        *e += 1;
    }

    let mut to_flash = Vec::new();
    loop {
        for c in octo.coords() {
            if octo[c] > 9 {
                to_flash.push(c);
                octo[c] = 0;
            }
        }

        if to_flash.is_empty() {
            break;
        }

        for (x, y) in to_flash.drain(..) {
            for dx in -1..=1 {
                for dy in -1..=1 {
                    octo.alter(x + dx, y + dy, |&e| if e == 0 { 0 } else { e + 1 });
                }
            }
        }
    }

    octo.iter().filter(|&&e| e == 0).count()
}

fn part1(i: Grid) -> usize {
    let mut i = i.map(|_, _, x| x.to_digit(10).unwrap());
    (1..=100).map(|_| step(&mut i)).sum()
}

fn part2(i: Grid) -> usize {
    let mut i = i.map(|_, _, x| x.to_digit(10).unwrap());
    (1..).find(|_| step(&mut i) == i.w * i.h).unwrap()
}

fn main() {
    run(part1);
    run(part2);
}
