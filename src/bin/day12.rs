use aoc::*;

#[derive(Debug, Inpt, Clone, Copy)]
#[inpt(regex = r"(\w+)-(\w+)")]
struct Edge<'s> {
    a: &'s str,
    b: &'s str,
}

const START: u8 = 0;
const END: u8 = 1;

#[derive(Debug)]
struct Node {
    to: Vec<u8>,
    small: bool,
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]

struct CountAt {
    node: u8,
    visited: u64,
    locked: bool,
}

#[derive(Default, Debug)]
struct Counter {
    adj: Vec<Node>,
}

impl Counter {
    fn add_node<'s>(&mut self, ids: &mut M<&'s str, u8>, name: &'s str) {
        match ids.entry(name) {
            Entry::Occupied(_) => (),
            Entry::Vacant(e) => {
                let id = self.adj.len() as u8;
                e.insert(id);
                self.adj.push(Node {
                    to: Vec::new(),
                    small: name.chars().all(|c| c.is_ascii_lowercase()),
                });
            }
        }
    }

    fn add_edges(&mut self, edges: Vec<Edge>) {
        let mut ids = M::default();
        self.add_node(&mut ids, "start");
        self.add_node(&mut ids, "end");
        for &Edge { a, b } in &edges {
            self.add_node(&mut ids, a);
            self.add_node(&mut ids, b);
        }

        for Edge { a, b } in edges {
            if b != "start" {
                self.adj[ids[a] as usize].to.push(ids[b]);
            }
            if a != "start" {
                self.adj[ids[b] as usize].to.push(ids[a]);
            }
        }
    }

    fn do_count(&self, at: CountAt, cache: &mut M<CountAt, usize>) -> usize {
        if at.node == END {
            return 1;
        }

        if let Some(&count) = cache.get(&at) {
            return count;
        }

        let prev_visited = at.visited.get_bit(at.node as usize);
        let at_node = &self.adj[at.node as usize];
        if at_node.small && prev_visited && at.locked {
            return 0;
        }

        let visited = at.visited.set_bit(at.node as usize, true);

        let count = at_node
            .to
            .iter()
            .map(|&node| {
                self.do_count(
                    CountAt {
                        node,
                        visited,
                        locked: at.locked || (at_node.small && prev_visited),
                    },
                    cache,
                )
            })
            .sum();
        cache.insert(at, count);
        return count;
    }

    fn count(&self, locked: bool) -> usize {
        self.do_count(
            CountAt {
                node: START,
                visited: 0,
                locked,
            },
            &mut M::default(),
        )
    }
}

fn part1(edges: Vec<Edge>) -> usize {
    let mut counter = Counter::default();
    counter.add_edges(edges);
    counter.count(true)
}

fn part2(edges: Vec<Edge>) -> usize {
    let mut counter = Counter::default();
    counter.add_edges(edges);
    counter.count(false)
}

fn main() {
    run(part1);
    run(part2);
}
