use aoc::*;

#[derive(Inpt, Debug, Clone, Copy)]
#[inpt(regex = r"fold along (\w)=")]
struct Fold {
    axis: char,
    #[inpt(after)]
    pos: usize,
}

#[derive(Debug, Inpt)]

struct Paper {
    dots: Group<Vec<(usize, usize)>>,
    folds: Vec<Fold>,
}

impl Paper {
    fn grid(&self) -> Grid<bool> {
        let w = self.dots.inner.iter().map(|(x, _)| *x).max().unwrap() + 1;
        let h = self.dots.inner.iter().map(|(_, y)| *y).max().unwrap() + 1;
        let mut grid = Grid::fill(w, h, |_, _| false);
        for &dot in &self.dots.inner {
            grid[dot] = true;
        }
        grid
    }
}

fn fold(paper: &mut Grid<bool>, Fold { axis, pos }: Fold) {
    if axis == 'x' {
        *paper = Grid::fill(pos, paper.h, |x, y| {
            paper[(x, y)] || paper[(2 * pos - x, y)]
        });
    } else {
        *paper = Grid::fill(paper.w, pos, |x, y| {
            paper[(x, y)] || paper[(x, 2 * pos - y)]
        });
    }
}

fn part1(directions: Paper) -> usize {
    let mut paper = directions.grid();
    fold(&mut paper, directions.folds[0]);
    paper.iter().filter(|b| **b).count()
}

fn part2(directions: Paper) -> Grid {
    let mut paper = directions.grid();
    for f in directions.folds {
        fold(&mut paper, f);
    }
    paper.map(|_, _, hole| if hole { '#' } else { ' ' })
}

fn main() {
    run(part1);
    run(part2);
}
