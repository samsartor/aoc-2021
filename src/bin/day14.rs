#![feature(array_windows)]

use aoc::*;

#[derive(Debug, Inpt)]
#[inpt(regex = r"(\w)(\w) -> (\w)")]
struct Replacement {
    a: char,
    b: char,
    r: char,
}

#[derive(Debug, Inpt)]
struct Input {
    #[inpt(split = "Group")]
    temp: Vec<char>,
    reps: Vec<Replacement>,
}

impl Input {
    fn run(self, steps: usize) -> usize {
        let mut lookup = M::default();
        for i in self.reps {
            lookup.insert([i.a, i.b], i.r);
        }

        let mut counts = self.temp.iter().copied().count_all();
        let mut pairs = self.temp.array_windows().copied().count_all();

        for _ in 0..steps {
            let mut new_pairs = M::default();
            new_pairs.reserve(pairs.len());
            for ([a, b], n) in pairs {
                if let Some(&r) = lookup.get(&[a, b]) {
                    *new_pairs.entry([a, r]).or_insert(0) += n;
                    *new_pairs.entry([r, b]).or_insert(0) += n;
                    *counts.entry(r).or_insert(0) += n;
                } else {
                    *new_pairs.entry([a, b]).or_insert(0) += n;
                }
            }
            pairs = new_pairs;
        }

        counts.values().max().unwrap() - counts.values().min().unwrap()
    }
}

fn part1(input: Input) -> usize {
    input.run(10)
}

fn part2(input: Input) -> usize {
    input.run(40)
}

fn main() {
    run(part1);
    run(part2);
}
