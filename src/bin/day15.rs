use aoc::*;
use std::{cmp::Reverse, collections::BinaryHeap};

fn part1(input: Grid) -> u32 {
    let map = input.map(|_, _, c| c.to_digit(10).unwrap());
    let mut q = BinaryHeap::new();
    let mut visited = Grid::fill(map.w, map.h, |_, _| 0);
    q.push(Reverse((0, 0isize, 0isize)));
    while let Some(Reverse((l, x, y))) = q.pop() {
        if x as usize == map.w - 1 && y as usize == map.h - 1 {
            return l;
        }
        if visited.get(x, y) != Some(&0) {
            continue;
        }
        visited.set(x, y, l);
        for (dx, dy) in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
            if let Some(v) = map.get(x + dx, y + dy) {
                q.push(Reverse((l + v, x + dx, y + dy)))
            }
        }
    }
    0
}

fn part2(input: Grid) -> u32 {
    let map = input.map(|_, _, c| c.to_digit(10).unwrap());
    let w = map.w as isize;
    let h = map.h as isize;
    let mut q = BinaryHeap::new();
    let mut visited = Grid::fill(map.w * 5, map.h * 5, |_, _| 0);
    q.push(Reverse((0, 0isize, 0isize)));
    while let Some(Reverse((l, x, y))) = q.pop() {
        if x == w * 5 - 1 && y == h * 5 - 1 {
            return l;
        }
        if visited.get(x, y) != Some(&0) {
            continue;
        }
        visited.set(x, y, l);
        for (dx, dy) in [(-1, 0), (0, -1), (0, 1), (1, 0)] {
            let x = x + dx;
            let y = y + dy;
            if let Some(v) = map.get(x % w, y % h) {
                let v = (v + (x / w) as u32 + (y / h) as u32 - 1) % 9 + 1;
                q.push(Reverse((l + v, x, y)))
            }
        }
    }
    0
}

fn main() {
    run(part1);
    run(part2);
}
