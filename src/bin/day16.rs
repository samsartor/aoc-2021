use aoc::*;

#[derive(Debug, Clone, Copy)]
enum Op {
    Lit(usize),
    Sum,
    Product,
    Min,
    Max,
    Gt,
    Lt,
    Eq,
}

#[derive(Debug, Clone)]
struct Expr {
    op: Op,
    args: Vec<usize>,
}

#[derive(Debug, Default, Inpt)]
#[inpt(from = "Hex")]
struct Program {
    version_sum: usize,
    exprs: M<usize, Expr>,
}

impl Program {
    fn append(&mut self, read: &mut BitReader) -> usize {
        let offset = read.offset;
        let version = read.pop_int(3);
        self.version_sum += version;
        let ty = read.pop_int(3);
        let expr = if ty == 4 {
            let mut lit = Vec::new();
            let mut more = true;
            while more {
                more = read.pop_bool();
                lit.extend_from_slice(read.pop_n(4).bits);
            }
            let lit = BitReader::new(0, &lit).int();
            Expr {
                op: Op::Lit(lit),
                args: Vec::new(),
            }
        } else {
            let mut args = Vec::new();
            if read.pop_bool() {
                let len = read.pop_int(11);
                for _ in 0..len {
                    args.push(self.append(read));
                }
            } else {
                let len = read.pop_int(15);
                let mut args_read = read.pop_n(len);
                while args_read.has() {
                    args.push(self.append(&mut args_read));
                }
            }

            let op = match ty {
                0 => Op::Sum,
                1 => Op::Product,
                2 => Op::Min,
                3 => Op::Max,
                5 => Op::Gt,
                6 => Op::Lt,
                7 => Op::Eq,
                _ => panic!(),
            };

            Expr { op, args }
        };
        self.exprs.insert(offset, expr);
        offset
    }

    pub fn args<'a>(&'a self, x: &'a Expr) -> impl Iterator<Item = usize> + 'a {
        x.args.iter().map(|&offset| self.eval(offset))
    }

    pub fn arg<'a>(&'a self, x: &'a Expr, index: usize) -> usize {
        self.eval(x.args[index])
    }

    pub fn eval(&self, offset: usize) -> usize {
        let x = &self.exprs[&offset];
        match x.op {
            Op::Lit(lit) => lit,
            Op::Sum => self.args(x).sum(),
            Op::Product => self.args(x).product(),
            Op::Min => self.args(x).min().unwrap(),
            Op::Max => self.args(x).max().unwrap(),
            Op::Gt => (self.arg(x, 0) > self.arg(x, 1)) as usize,
            Op::Lt => (self.arg(x, 0) < self.arg(x, 1)) as usize,
            Op::Eq => (self.arg(x, 0) == self.arg(x, 1)) as usize,
        }
    }
}

impl From<Hex> for Program {
    fn from(hex: Hex) -> Self {
        let mut this = Program::default();
        this.append(&mut hex.read());
        this
    }
}

fn part1(p: Program) -> usize {
    p.version_sum
}

fn part2(p: Program) -> usize {
    p.eval(0)
}

fn main() {
    run(part1);
    run(part2);
}
