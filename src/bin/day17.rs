#![feature(array_windows)]

use aoc::*;

#[derive(Inpt, Debug)]
#[inpt(regex = r"target area: x=([-\d]+)..([-\d]+), y=([-\d]+)..([-\d]+)")]
struct Input {
    x0: i32,
    x1: i32,
    y0: i32,
    y1: i32,
}

fn part1(i: Input) -> i32 {
    i.y0 * (i.y0 + 1) / 2
}

fn part2(i: Input) -> usize {
    let mut count = 0;
    for dx in 1..=i.x1 {
        for dy in i.y0..=-i.y0 {
            let mut sdx = dx;
            let mut sdy = dy;
            let mut x = 0;
            let mut y = 0;
            loop {
                if y < i.y0 || x > i.x1 {
                    break;
                }

                if x >= i.x0 && x <= i.x1 && y >= i.y0 && y <= i.y1 {
                    count += 1;
                    break;
                }

                x += sdx;
                y += sdy;

                sdy -= 1;
                if sdx > 0 {
                    sdx -= 1;
                }
            }
        }
    }
    count
}

fn main() {
    run(part1);
    run(part2);
}
