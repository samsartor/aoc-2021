use aoc::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tok {
    Pair,
    Lit(u32),
}

#[derive(Inpt, Debug, Clone, PartialEq, Eq)]
#[inpt(from = "Line<&'s str>")]
struct Toks {
    pub vec: Vec<Tok>,
}

impl<'s> From<&'s str> for Toks {
    fn from(num: &'s str) -> Self {
        let vec = num
            .chars()
            .filter_map(|c| match (c, c.to_digit(10)) {
                ('[', _) => Some(Tok::Pair),
                (_, Some(d)) => Some(Tok::Lit(d)),
                _ => None,
            })
            .collect();
        Toks { vec }
    }
}

impl<'s> From<Line<&'s str>> for Toks {
    fn from(Line { inner }: Line<&'s str>) -> Self {
        inner.into()
    }
}

impl Toks {
    fn add(&mut self, b: &Toks) {
        self.vec.insert(0, Tok::Pair);
        self.vec.extend_from_slice(&b.vec);
        self.reduce();
    }

    fn reduce_once(&mut self) -> bool {
        let x = &mut self.vec;
        let mut indexes = Vec::new();
        for i in 0..x.len() {
            while let Some(2) = indexes.last() {
                indexes.pop();
            }
            if let Some(i) = indexes.last_mut() {
                *i += 1;
            }
            if matches!(x[i], Tok::Pair) {
                indexes.push(0);
            }

            if indexes.len() > 4 {
                match x[i..][..3] {
                    [Tok::Pair, Tok::Lit(_), Tok::Lit(_)] => (),
                    _ => continue,
                }
                x[i] = Tok::Lit(0);
                let a = match x.remove(i + 1) {
                    Tok::Pair => panic!(),
                    Tok::Lit(a) => a,
                };
                let b = match x.remove(i + 1) {
                    Tok::Pair => panic!(),
                    Tok::Lit(b) => b,
                };
                for j in (0..i).rev() {
                    if let Tok::Lit(y) = &mut x[j] {
                        *y += a;
                        break;
                    }
                }
                for j in (i + 1)..x.len() {
                    if let Tok::Lit(y) = &mut x[j] {
                        *y += b;
                        break;
                    }
                }
                return true;
            }
        }

        for i in 0..x.len() {
            let y = match x[i] {
                Tok::Pair => continue,
                Tok::Lit(y) => y,
            };
            if y > 9 {
                x[i] = Tok::Pair;
                x.insert(i + 1, Tok::Lit(y / 2));
                x.insert(i + 2, Tok::Lit((y + 1) / 2));
                return true;
            }
        }

        false
    }

    fn reduce(&mut self) {
        while self.reduce_once() {}
    }

    fn mag(&self) -> u64 {
        fn mag_impl(x: &[Tok], at: &mut usize) -> u64 {
            match x[*at] {
                Tok::Pair => {
                    *at += 1;
                    3 * mag_impl(x, at) + 2 * mag_impl(x, at)
                }
                Tok::Lit(d) => {
                    *at += 1;
                    d as u64
                }
            }
        }

        let mut at = 0;
        mag_impl(&self.vec, &mut at)
    }
}

#[test]
fn test_reduce() {
    fn assert_reduces(x: &str, b: &str) {
        let mut x: Toks = x.into();
        x.reduce_once();
        assert_eq!(x, b.into());
    }

    assert_reduces("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
    assert_reduces("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
    assert_reduces("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
    assert_reduces(
        "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]",
        "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
    );
    assert_reduces(
        "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
        "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
    );

    assert_reduces(
        "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]",
        "[[[[0,7],4],[7,[[8,4],9]]],[1,1]]",
    );
}

fn part1(mut nums: Vec<Toks>) -> u64 {
    let mut num = nums.remove(0);
    for other in &nums {
        num.add(other);
    }
    num.mag()
}

fn part2(nums: Vec<Toks>) -> u64 {
    let mut max = 0;
    for a in &nums {
        for b in &nums {
            if a != b {
                let mut x = a.clone();
                x.add(b);
                max = x.mag().max(max);
            }
        }
    }
    max
}

fn main() {
    run(part1);
    run(part2);
}
