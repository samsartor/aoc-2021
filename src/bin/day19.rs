use aoc::*;
use std::{
    collections::BTreeSet,
    iter::{once, repeat},
};

type C = [i32; 3];
type Coords = BTreeSet<C>;

#[derive(Inpt, Debug)]
struct Scan<'s> {
    _intro: Line<&'s str>,
    coords: Vec<(i32, i32, i32)>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Axis {
    NegX,
    PosX,
    NegY,
    PosY,
    NegZ,
    PosZ,
}

use Axis::*;

impl Axis {
    const ALL: [Axis; 6] = [NegX, PosX, NegY, PosY, NegZ, PosZ];

    fn x(self) -> i32 {
        match self {
            NegX => -1,
            PosX => 1,
            _ => 0,
        }
    }

    fn y(self) -> i32 {
        match self {
            NegY => -1,
            PosY => 1,
            _ => 0,
        }
    }

    fn z(self) -> i32 {
        match self {
            NegZ => -1,
            PosZ => 1,
            _ => 0,
        }
    }

    fn cross(self, other: Axis) -> Option<Axis> {
        let x = self.y() * other.z() - self.z() * other.y();
        if x == -1 {
            return Some(NegX);
        }
        if x == 1 {
            return Some(PosX);
        }
        let y = self.z() * other.x() - self.x() * other.z();
        if y == -1 {
            return Some(NegY);
        }
        if y == 1 {
            return Some(PosY);
        }
        let z = self.x() * other.y() - self.y() * other.x();
        if z == -1 {
            return Some(NegZ);
        }
        if z == 1 {
            return Some(PosZ);
        }
        None
    }
}

#[derive(Debug, Clone, Copy)]
struct Transform {
    source: [Axis; 3],
    offset: [i32; 3],
}

impl Default for Transform {
    fn default() -> Self {
        Self {
            source: [PosX, PosY, PosZ],
            offset: [0, 0, 0],
        }
    }
}

fn zip<T>(a: C, b: C, mut f: impl FnMut(i32, i32) -> T) -> [T; 3] {
    [f(a[0], b[0]), f(a[1], b[1]), f(a[2], b[2])]
}

impl Transform {
    fn apply(&self, coords: C, point: bool) -> C {
        let axis = |i: usize| -> i32 {
            let v = match self.source[i] {
                NegX => -coords[0],
                PosX => coords[0],
                NegY => -coords[1],
                PosY => coords[1],
                NegZ => -coords[2],
                PosZ => coords[2],
            };
            if point {
                v + self.offset[i]
            } else {
                v
            }
        };
        [axis(0), axis(1), axis(2)]
    }

    fn apply_all(&self, coords: &Coords) -> Coords {
        coords
            .iter()
            .map(|coords| self.apply(*coords, true))
            .collect()
    }

    fn from_pair(a0: C, a1: C, b0: C, b1: C) -> Count<Transform> {
        let a_delta = zip(a1, a0, |x1, x0| x1 - x0);
        let b_delta = zip(b1, b0, |x1, x0| x1 - x0);
        let mut out = Count::None;
        for x_source in Axis::ALL {
            for y_source in Axis::ALL {
                if let Some(z_source) = x_source.cross(y_source) {
                    let mut t = Transform {
                        source: [x_source, y_source, z_source],
                        offset: [0; 3],
                    };
                    if t.apply(a_delta, false) != b_delta {
                        continue;
                    }
                    t.offset = zip(b0, t.apply(a0, false), |x1, x0| x1 - x0);
                    out.push(t);
                }
            }
        }
        out
    }
}

#[derive(Clone, Copy, Debug)]
enum Count<T> {
    None,
    One(T),
    Many,
}

impl<T> Count<T> {
    pub fn push(&mut self, val: T) {
        *self = match self {
            Count::None => Count::One(val),
            Count::One(_) => Count::Many,
            Count::Many => Count::Many,
        }
    }
}

fn solve(scans: Vec<Group<Scan>>) -> (Vec<Coords>, Vec<Option<(usize, Transform)>>) {
    let sets: Vec<Coords> = scans
        .iter()
        .map(|scan| {
            scan.inner
                .coords
                .iter()
                .map(|&(x, y, z)| [x, y, z])
                .collect()
        })
        .collect();

    let mut dists = M::default();
    for (id, scan) in scans.into_iter().enumerate() {
        let coords = &scan.inner.coords;
        for i in 0..coords.len() {
            for j in 0..coords.len() {
                if i == j {
                    continue;
                }
                let a = coords[i];
                let b = coords[j];
                let a = [a.0, a.1, a.2];
                let b = [b.0, b.1, b.2];
                let mut d = zip(a, b, |a, b| (b - a).abs());
                d.sort();
                dists.entry(d).or_insert(Vec::new()).push((id, a, b));
            }
        }
    }
    let mut lookup = vec![Vec::new(); sets.len()];
    for (_, scans) in dists {
        for &(old, old_a, old_b) in &scans {
            for &(new, new_a, new_b) in &scans {
                if old == new {
                    continue;
                }
                if let Count::One(t) = Transform::from_pair(old_a, old_b, new_a, new_b) {
                    let mapped = t.apply_all(&sets[old]);
                    let n = mapped.intersection(&sets[new]).count();
                    if n >= 12 {
                        lookup[new].push((old, t));
                    }
                }
            }
        }
    }

    let mut acyclic_lookup = vec![None; sets.len()];
    let mut stack = vec![sets.len() - 1];
    while let Some(new) = stack.last().copied() {
        if let Some((old, t)) = lookup[new].pop() {
            if !stack.contains(&old) {
                stack.push(old);
                acyclic_lookup[old] = Some((new, t));
            }
        } else {
            stack.pop();
        }
    }

    (sets, acyclic_lookup)
}

fn part1(scans: Vec<Group<Scan>>) -> usize {
    let (mut sets, lookup) = solve(scans);

    while sets.iter().filter(|set| !set.is_empty()).count() > 1 {
        for (old, to) in lookup.iter().enumerate() {
            if let Some((new, t)) = *to {
                let mut mapped = t.apply_all(&sets[old]);
                sets[old].clear();
                sets[new].append(&mut mapped);
            }
        }
    }

    sets.last().unwrap().len()
}

fn part2(scans: Vec<Group<Scan>>) -> i32 {
    let (sets, lookup) = solve(scans);
    let mut sets: Vec<Coords> = repeat(once([0, 0, 0]).collect()).take(sets.len()).collect();
    while sets.iter().filter(|set| !set.is_empty()).count() > 1 {
        for (old, to) in lookup.iter().enumerate() {
            if let Some((new, t)) = *to {
                let mut mapped = t.apply_all(&sets[old]);
                sets[old].clear();
                sets[new].append(&mut mapped);
            }
        }
    }

    let set = sets.last().unwrap();
    let mut max_dist = 0;
    for &a in set {
        for &b in set {
            let dist = zip(a, b, |a, b| (a - b).abs()).into_iter().sum();
            max_dist = max_dist.max(dist);
        }
    }

    max_dist
}

fn main() {
    run(part1);
    run(part2);
}
