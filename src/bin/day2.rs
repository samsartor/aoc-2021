#![feature(array_windows)]

use aoc::*;

fn part1(input: Vec<(Word<&str>, usize)>) -> usize {
    let mut x = 0;
    let mut z = 0;
    for (dir, v) in input {
        match dir.inner {
            "forward" => x += v,
            "down" => z += v,
            "up" => z -= v,
            _ => panic!(),
        }
    }
    x * z
}

fn part2(input: Vec<(Word<&str>, usize)>) -> usize {
    let mut x = 0;
    let mut a = 0;
    let mut z = 0;
    for (dir, v) in input {
        match dir.inner {
            "forward" => {
                x += v;
                z += a * v;
            }
            "down" => a += v,
            "up" => a -= v,
            _ => panic!(),
        }
    }
    x * z
}

fn main() {
    run(part1);
    run(part2);
}
