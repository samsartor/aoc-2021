use aoc::*;

#[derive(Inpt, Debug)]
struct Input {
    mapping: Line<String>,
    image: Grid,
}

fn enhance(i: Input, steps: usize) -> usize {
    let mut image = i.image;
    let mapping = i.mapping.inner;
    let mut other = '.';
    for _ in 0..steps {
        image = Grid::fill(image.w + 2, image.h + 2, |x, y| {
            let x = x as isize - 1;
            let y = y as isize - 1;
            let mut index = 0;
            for dy in -1..=1 {
                for dx in -1..=1 {
                    index <<= 1;
                    if *image.get(x + dx, y + dy).unwrap_or(&other) == '#' {
                        index |= 1;
                    }
                }
            }
            mapping.at(index)
        });
        other = mapping.at(if other == '.' { 0 } else { 0b111_111_111 });
    }
    image.iter().filter(|p| **p == '#').count()
}

fn part1(i: Input) -> usize {
    enhance(i, 2)
}

fn part2(i: Input) -> usize {
    enhance(i, 50)
}

fn main() {
    run(part1);
    run(part2);
}
