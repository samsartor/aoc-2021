use aoc::*;

#[derive(Inpt, Debug)]
#[inpt(regex = r"Player 1 starting position: (\d+)\sPlayer 2 starting position: (\d+)")]
struct Input {
    p1: u32,
    p2: u32,
}

fn part1(Input { mut p1, mut p2 }: Input) -> u32 {
    let mut rolls = 0;
    let mut dice = 1;
    let mut roll = || {
        rolls += 1;
        let next = (dice % 100) + 1;
        replace(&mut dice, next)
    };
    let mut s1 = 0;
    let mut s2 = 0;
    loop {
        p1 += roll();
        p1 += roll();
        p1 += roll();
        p1 = (p1 - 1) % 10 + 1;
        s1 += p1;
        if s1 >= 1000 {
            break;
        }
        p2 += roll();
        p2 += roll();
        p2 += roll();
        p2 = (p2 - 1) % 10 + 1;
        s2 += p2;
        if s2 >= 1000 {
            break;
        }
    }
    s1.min(s2) * rolls
}

fn wins(s1: u8, s2: u8, p1: u8, p2: u8, cache: &mut M<([u8; 4]), (u64, u64)>) -> (u64, u64) {
    let key = [s1, s2, p1, p2];
    if let Some(w) = cache.get(&key) {
        return *w;
    }

    let mut w1 = 0;
    let mut w2 = 0;
    for (d, dn) in [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)] {
        let mut s = s1;
        let mut p = p1;
        p += d;
        p = (p - 1) % 10 + 1;
        s += p;
        if s >= 21 {
            w1 += dn;
        } else {
            let (v1, v2) = wins(s2, s, p2, p, cache);
            w1 += dn * v2;
            w2 += dn * v1;
        }
    }

    cache.insert(key, (w1, w2));
    (w1, w2)
}

fn part2(Input { p1, p2 }: Input) -> u64 {
    let mut cache = M::default();
    let (w1, w2) = wins(0, 0, p1 as u8, p2 as u8, &mut cache);
    w1.max(w2)
}

fn main() {
    run(part1);
    run(part2);
}
