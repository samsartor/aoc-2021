use aoc::*;

#[derive(Clone, Copy, Debug, Inpt)]
#[inpt(regex = r"\.\.")]
struct Range {
    #[inpt(before)]
    start: i64,
    #[inpt(after)]
    end: i64,
}

#[derive(Inpt, Debug)]
#[inpt(regex = r"(on|off) x=([^,]+),y=([^,]+),z=([^\s]+)")]
struct Input<'s> {
    com: &'s str,
    x: Range,
    y: Range,
    z: Range,
}

fn part1(inputs: Vec<Input>) -> usize {
    let mut count = 0;
    const B: i64 = 50;
    for x in -B..=B {
        for y in -B..=B {
            for z in -B..=B {
                if let Some(i) = inputs
                    .iter()
                    .rev()
                    .find(|i| i.x.contains(x) && i.y.contains(y) && i.z.contains(z))
                {
                    if i.com == "on" {
                        count += 1;
                    }
                }
            }
        }
    }
    count
}

impl Range {
    fn contains(self, x: i64) -> bool {
        self.start <= x && x <= self.end
    }

    fn intersect(self: Range, b: Range) -> Range {
        Range {
            start: self.start.max(b.start),
            end: self.end.min(b.end),
        }
    }

    fn len(self) -> i64 {
        (self.end - self.start + 1).max(0)
    }
}

#[derive(Clone, Copy, Debug)]
struct Cube {
    sub: bool,
    x: Range,
    y: Range,
    z: Range,
}

impl Cube {
    fn cancel_intersect(&self, other: &Cube) -> Cube {
        Cube {
            sub: !self.sub,
            x: self.x.intersect(other.x),
            y: self.y.intersect(other.y),
            z: self.z.intersect(other.z),
        }
    }

    fn volume(&self) -> i64 {
        let v = self.x.len() * self.y.len() * self.z.len();
        if self.sub {
            -v
        } else {
            v
        }
    }
}

fn part2(i: Vec<Input>) -> i64 {
    let mut cubes = Vec::<Cube>::new();
    for Input { com, x, y, z } in i {
        let mut new = cubes.clone();
        let b = Cube {
            sub: false,
            x,
            y,
            z,
        };
        for a in cubes {
            let c = a.cancel_intersect(&b);
            if c.volume() != 0 {
                new.push(c);
            }
        }
        if com == "on" {
            new.push(b);
        }
        cubes = new;
    }
    cubes.into_iter().map(|c| c.volume()).sum()
}

fn main() {
    run(part1);
    run(part2);
}
