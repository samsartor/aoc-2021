use aoc::*;
use smallvec::SmallVec;
use std::{cmp::Ordering, collections::BinaryHeap, hash::Hash};

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct Room<const R: usize> {
    waiting: SmallVec<[char; R]>,
    completed: usize,
}

impl<const R: usize> Room<R> {
    pub fn new(i: usize, a: char, b: char) -> Self {
        let mut waiting = SmallVec::new();
        waiting.push(a);
        if R == 4 {
            waiting.extend_from_slice(&match i {
                0 => ['D', 'D'],
                1 => ['C', 'B'],
                2 => ['B', 'A'],
                3 => ['A', 'C'],
                _ => panic!(),
            });
        }
        waiting.push(b);
        let mut completed = 0;
        let c = room_pod(i);
        while waiting.last() == Some(&c) {
            completed += 1;
            waiting.pop();
        }
        waiting.reverse();
        Room { waiting, completed }
    }
}

#[derive(Clone, Debug, Inpt)]
#[inpt(from = "Vec<char>")]
struct State<const R: usize> {
    cost: u32,
    hallway: [char; 7],
    rooms: [Room<R>; 4],
}

fn cost(c: char) -> u32 {
    match c {
        'A' => 1,
        'B' => 10,
        'C' => 100,
        'D' => 1000,
        _ => panic!(),
    }
}

fn room_pod(room: usize) -> char {
    ('A' as u32 + room as u32).try_into().unwrap()
}

impl<const R: usize> State<R> {
    fn pop_room(&self, room: usize) -> Option<(State<R>, char)> {
        let mut this = self.clone();
        let distance = R - (this.rooms[room].waiting.len() + this.rooms[room].completed);
        let pod = this.rooms[room].waiting.pop()?;
        this.cost += distance as u32 * cost(pod);
        Some((this, pod))
    }

    fn push_room(&self, room: usize) -> Option<State<R>> {
        if !self.rooms[room].waiting.is_empty() {
            return None;
        }
        let mut this = self.clone();
        this.rooms[room].completed += 1;
        let distance = R - this.rooms[room].completed;
        this.cost += distance as u32 * cost(room_pod(room));
        Some(this)
    }

    fn is_end(&self) -> bool {
        self.rooms.iter().all(|r| r.completed == R)
    }
}

impl<const R: usize> PartialEq for State<R> {
    fn eq(&self, other: &Self) -> bool {
        self.hallway == other.hallway && self.rooms == other.rooms
    }
}

impl<const R: usize> Hash for State<R> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hallway.hash(state);
        self.rooms.hash(state);
    }
}

impl<const R: usize> Eq for State<R> {}

impl<const R: usize> PartialOrd for State<R> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.cost.partial_cmp(&other.cost).map(Ordering::reverse)
    }
}

impl<const R: usize> Ord for State<R> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.cost.cmp(&other.cost).reverse()
    }
}

impl<const R: usize> State<R> {
    fn visit(&self, mut each: impl FnMut(State<R>)) {
        for i in 0..4 {
            let pushed = self.push_room(i);
            let popped = self.pop_room(i);
            let mut case = |n, j| {
                let mut distance = (n + 1) as u32 * 2;
                if j == 0 || j == 6 {
                    distance -= 1;
                }
                if self.hallway[j] == '.' {
                    if let Some((ref new, v)) = popped {
                        let mut new = new.clone();
                        new.cost += distance * cost(v);
                        new.hallway[j] = v;
                        each(new);
                    }
                } else if self.hallway[j] == room_pod(i) {
                    if let Some(ref new) = pushed {
                        let mut new = new.clone();
                        new.cost += distance * cost(room_pod(i));
                        new.hallway[j] = '.';
                        each(new);
                    }
                }
            };
            for (n, j) in (0..=i + 1).rev().enumerate() {
                case(n, j);
                if self.hallway[j] != '.' {
                    break;
                }
            }
            for (n, j) in (i + 2..=6).enumerate() {
                case(n, j);
                if self.hallway[j] != '.' {
                    break;
                }
            }
        }
    }
}

impl<const R: usize> From<Vec<char>> for State<R> {
    fn from(mut rooms: Vec<char>) -> Self {
        rooms.retain(|c| c.is_alphanumeric());
        State {
            cost: 0,
            hallway: ['.'; 7],
            rooms: [0, 1, 2, 3].map(|i| Room::new(i, rooms[i], rooms[4 + i])),
        }
    }
}

fn solve<const R: usize>(i: State<R>) -> u32 {
    let mut queue = BinaryHeap::new();
    queue.push(i.clone());
    let mut visited = S::default();
    while let Some(state) = queue.pop() {
        if !visited.insert(state.clone()) {
            continue;
        }
        if state.is_end() {
            return state.cost;
        }
        state.visit(|new| {
            queue.push(new);
        });
    }
    panic!()
}

fn part1(i: State<2>) -> u32 {
    solve(i)
}

fn part2(i: State<4>) -> u32 {
    solve(i)
}

fn main() {
    run(part1);
    run(part2);
}
