#![feature(int_log)]

use aoc::*;

#[derive(Inpt, Debug, Clone, Copy)]
#[inpt(regex = "\
inp w
mul x 0
add x z
mod x 26
div z (26|1)
add x (-?\\d+)
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y (-?\\d+)
mul y x
add z y")]
struct Step {
    div: i32,
    check: i32,
    offset: i32,
}

fn solve(ops: &[Step], z: i32, digits: impl Iterator<Item = i32> + Clone) -> Option<Vec<i32>> {
    if match z {
        0 => 0,
        z => z.ilog(26) as usize,
    } > ops
        .iter()
        .filter(|op| op.div == 26 || op.check < 10)
        .count()
    {
        return None;
    }

    let Step { div, check, offset } = match ops.get(0) {
        Some(op) => op,
        None => return if z == 0 { Some(Vec::new()) } else { None },
    };

    for dig in digits.clone() {
        let res = if z % 26 + check == dig {
            solve(&ops[1..], z / div, digits.clone())
        } else {
            solve(&ops[1..], (z / div * 26) + dig + offset, digits.clone())
        };
        if let Some(mut digits) = res {
            digits.push(dig);
            return Some(digits);
        }
    }

    None
}

fn solve_all(steps: &[Step], digits: impl Iterator<Item = i32> + Clone) -> String {
    let mut digits = solve(&steps, 0, digits).unwrap();
    digits.reverse();
    digits
        .into_iter()
        .map(|d| char::from_digit(d as u32, 10).unwrap())
        .collect()
}

fn part1(steps: Vec<Step>) -> String {
    solve_all(&steps, (1..=9).rev())
}

fn part2(steps: Vec<Step>) -> String {
    solve_all(&steps, 1..=9)
}

fn main() {
    run(part1);
    run(part2);
}
