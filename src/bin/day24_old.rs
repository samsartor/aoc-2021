use aoc::*;

#[derive(Inpt, Debug, Clone, Copy, PartialEq, Eq)]
enum Register {
    #[inpt(regex = r"([a-z])")]
    Named { name: char },
    #[inpt(regex = r"(-?\d+)")]
    Lit { value: i32 },
}

#[derive(Inpt, Debug, Clone, Copy, PartialEq, Eq)]
enum Inst<'s> {
    #[inpt(regex = r"(\w+) ([a-z]) (-?\w+)\s*")]
    Expr {
        op: &'s str,
        output: char,
        input: Register,
    },
    #[inpt(regex = r"inp ([a-z])\s*")]
    Input { output: char },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Op {
    Dig(usize),
    Val(i32),
    Add(usize, usize),
    Mul(usize, usize),
    Div(usize, usize),
    Mod(usize, usize),
    Eql(usize, usize),
    Nop,
}

use Op::*;

fn optimize(ops: &mut [Op]) {
    for i in 0..ops.len() {
        match ops[i] {
            Add(a, b) => match (ops[a], ops[b]) {
                (Val(va), Val(vb)) => ops[i] = Val(va + vb),
                (op, Val(0)) | (Val(0), op) => ops[i] = op,
                _ => (),
            },
            Mul(a, b) => match (ops[a], ops[b]) {
                (Val(va), Val(vb)) => ops[i] = Val(va * vb),
                (_, Val(0)) => ops[i] = Val(0),
                (op, Val(1)) | (Val(1), op) => ops[i] = op,
                _ => (),
            },
            Div(a, b) => match (ops[a], ops[b]) {
                (Val(va), Val(vb)) => ops[i] = Val(va / vb),
                (op, Val(1)) | (Val(1), op) => ops[i] = op,
                _ => (),
            },
            Mod(a, b) => match (ops[a], ops[b]) {
                (Val(va), Val(vb)) => ops[i] = Val(va % vb),
                (_, Val(0) | Val(1)) => ops[i] = Val(0),
                _ => (),
            },

            Eql(a, b) => match (ops[a], ops[b]) {
                (Val(va), Val(vb)) => ops[i] = Val(if va == vb { 1 } else { 0 }),
                (Dig(_), Val(v)) | (Val(v), Dig(_)) if v < 1 || v > 10 => ops[i] = Val(0),
                _ => (),
            },
            _ => (),
        }
    }

    // Find unique operations
    let mut unique = M::default();
    let mut dedup = Vec::new();
    for (i, &op) in ops.iter().enumerate() {
        match unique.entry(op) {
            Entry::Occupied(e) => {
                dedup.push(*e.get());
            }
            Entry::Vacant(e) => {
                e.insert(i);
                dedup.push(i);
            }
        }
    }

    // Deduplicate operations
    for op in ops.iter_mut() {
        match op {
            Add(a, b) | Mul(a, b) | Div(a, b) | Mod(a, b) | Eql(a, b) => {
                *a = dedup[*a];
                *b = dedup[*b];
            }
            _ => (),
        }
    }

    // Find used operations
    let mut used = S::default();
    used.insert(ops.len() - 1);
    for (i, op) in ops.iter().enumerate().rev() {
        if used.contains(&i) {
            match *op {
                Add(a, b) | Mul(a, b) | Div(a, b) | Mod(a, b) | Eql(a, b) => {
                    used.insert(a);
                    used.insert(b);
                }
                _ => (),
            }
        }
    }

    // Set unused operations to no-ops
    for (i, op) in ops.iter_mut().enumerate() {
        if !used.contains(&i) {
            *op = Nop;
        }
    }
}

fn reduce(ops: &[Op]) -> Vec<Op> {
    let mut new = Vec::new();
    let mut map = M::default();
    for (i, op) in ops.iter().enumerate() {
        if let Nop = op {
            continue;
        }
        map.insert(i, new.len());
        new.push(match *op {
            Add(a, b) => Add(map[&a], map[&b]),
            Mul(a, b) => Mul(map[&a], map[&b]),
            Div(a, b) => Div(map[&a], map[&b]),
            Mod(a, b) => Mod(map[&a], map[&b]),
            Eql(a, b) => Eql(map[&a], map[&b]),
            op => op,
        });
    }
    new
}

fn print(ops: &[Op]) {
    let mut counts = vec![0; ops.len()];
    for op in ops.iter() {
        match *op {
            Add(a, b) | Mul(a, b) | Div(a, b) | Mod(a, b) | Eql(a, b) => {
                counts[a] += 1;
                counts[b] += 1;
            }
            _ => (),
        }
    }
    *counts.last_mut().unwrap() = usize::MAX;

    let mut i = 0;
    let mut strs = Vec::new();
    for (op, c) in ops.iter().zip(counts) {
        strs.push(match *op {
            Dig(d) => {
                strs.push(format!("#{}", d));
                continue;
            }
            Val(v) => {
                strs.push(v.to_string());
                continue;
            }
            Add(a, b) => format!("(+ {} {})", strs[a], strs[b]),
            Mul(a, b) => format!("(* {} {})", strs[a], strs[b]),
            Div(a, b) => format!("(/ {} {})", strs[a], strs[b]),
            Mod(a, b) => format!("(% {} {})", strs[a], strs[b]),
            Eql(a, b) => format!("(= {} {})", strs[a], strs[b]),
            Nop => {
                strs.push(String::new());
                continue;
            }
        });

        if c > 1 {
            let expr = replace(strs.last_mut().unwrap(), format!("${:02}", i));
            eprintln!("{:02}: {}", i, expr);
            i += 1;
        }
    }
}

fn part1(i: Vec<Inst>) -> String {
    fn idx(reg: char) -> usize {
        reg as usize - 'w' as usize
    }

    let mut ops = vec![Val(0), Val(0), Val(0), Val(0)];
    let mut values = [0, 1, 2, 3];
    let mut push = |op| {
        let idx = ops.len();
        ops.push(op);
        idx
    };

    let mut dig = 0;
    for inst in i {
        match inst {
            Inst::Expr { op, output, input } => {
                let a = values[idx(output)];
                let b = match input {
                    Register::Named { name } => values[idx(name)],
                    Register::Lit { value } => push(Val(value)),
                };
                values[idx(output)] = push(match op {
                    "add" => Add(a, b),
                    "mul" => Mul(a, b),
                    "div" => Div(a, b),
                    "mod" => Mod(a, b),
                    "eql" => Eql(a, b),
                    _ => panic!("unsupported {}", op),
                });
            }
            Inst::Input { output } => {
                values[idx(output)] = push(Dig(dig));
                dig += 1;
            }
        }
    }

    optimize(&mut ops);
    ops = reduce(&ops);
    print(&ops);

    let _ctx = z3::Context::new(&z3::Config::default());

    String::new()
}

fn part2(_i: Vec<Inst>) -> usize {
    0
}

fn main() {
    run(part1);
    // run(part2);
}
