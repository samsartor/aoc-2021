use aoc::*;

fn part1(mut i: Grid) -> usize {
    let mut time = 0;
    let mut new: Grid;
    loop {
        let mut moved = false;

        new = Grid::fill(i.w, i.h, |_, _| '.');
        for (x, y) in i.coords() {
            let x1 = (x + 1) % i.w as isize;
            if i[(x, y)] == '>' {
                if i[(x1, y)] == '.' {
                    new[(x1, y)] = '>';
                    moved = true;
                } else {
                    new[(x, y)] = '>';
                }
            } else if i[(x, y)] == 'v' {
                new[(x, y)] = 'v';
            }
        }
        i = new;

        new = Grid::fill(i.w, i.h, |_, _| '.');
        for (x, y) in i.coords() {
            let y1 = (y + 1) % i.h as isize;
            if i[(x, y)] == 'v' {
                if i[(x, y1)] == '.' {
                    new[(x, y1)] = 'v';
                    moved = true;
                } else {
                    new[(x, y)] = 'v';
                }
            } else if i[(x, y)] == '>' {
                new[(x, y)] = '>';
            }
        }
        i = new;

        time += 1;
        if !moved {
            return time;
        }
    }
}

fn main() {
    run(part1);
}
