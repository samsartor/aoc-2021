use aoc::*;

fn part1(report: Grid) -> usize {
    let mut ones = vec![0; report.w];
    for i in 0..report.h {
        for j in 0..report.w {
            if report.atu(j, i) == '1' {
                ones[j] += 1;
            }
        }
    }

    let mut gamma = 0;
    let mut epsilon = 0;
    for j in 0..report.w {
        gamma <<= 1;
        epsilon <<= 1;
        if ones[j] > report.h / 2 {
            gamma |= 1;
        } else {
            epsilon |= 1;
        }
    }

    gamma * epsilon
}

fn part2(report: Vec<Line<&str>>) -> usize {
    fn filter(mut numbers: Vec<Line<&str>>, most: bool) -> usize {
        let mut j = 0;
        while numbers.len() > 1 {
            let mut ones = 0;
            for &num in &numbers {
                if num.inner.at(j) == '1' {
                    ones += 1;
                }
            }

            let to_match = if most == (ones >= numbers.len() - ones) {
                '1'
            } else {
                '0'
            };

            numbers.retain(|&i| i.inner.at(j) == to_match);
            j += 1;
        }
        usize::from_str_radix(numbers[0].inner, 2).unwrap()
    }

    filter(report.clone(), true) * filter(report.clone(), false)
}

fn main() {
    run(part1);
    run(part2);
}
