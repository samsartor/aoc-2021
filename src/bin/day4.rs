use aoc::*;

#[derive(Inpt, Debug)]
struct Bingos {
    #[inpt(split = "Line")]
    numbers: Vec<usize>,
    boards: Vec<Grid<usize>>,
}

fn wins(Bingos { numbers, boards }: Bingos) -> Vec<usize> {
    let mut boards: Vec<_> = boards
        .into_iter()
        .map(|b| b.map(|_, _, n| (n, false)))
        .collect();

    let mut wins = Vec::new();
    for num in numbers {
        for board in &mut boards {
            for (cell, marked) in board.iter_mut() {
                if *cell == num {
                    *marked = true;
                }
            }
        }

        boards.retain_mut(|board| {
            let mut won = false;
            for i in 0..5 {
                won = won || board.cast(i, 0, 0, 1).all(|&(_, marked)| marked);
                won = won || board.cast(0, i, 1, 0).all(|&(_, marked)| marked);
            }

            if won {
                let sum = board
                    .iter()
                    .filter(|&(_, checked)| !checked)
                    .map(|&(cell, _)| cell)
                    .sum::<usize>();
                wins.push(sum * num);
                false
            } else {
                true
            }
        })
    }

    wins
}

fn part1(input: Bingos) -> usize {
    *wins(input).first().unwrap()
}

fn part2(input: Bingos) -> usize {
    *wins(input).last().unwrap()
}

fn main() {
    run(part1);
    run(part2);
}
