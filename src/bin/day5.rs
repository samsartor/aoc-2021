use aoc::*;
use std::cmp::Ordering::*;

#[derive(Inpt, Debug)]
#[inpt(regex = r"(\d+),(\d+) -> (\d+),(\d+)")]
struct Vent {
    x0: usize,
    y0: usize,
    x1: usize,
    y1: usize,
}

fn solve(vents: Vec<Vent>) -> usize {
    let w = vents.iter().flat_map(|v| [v.x0, v.x1]).max().unwrap() + 1;
    let h = vents.iter().flat_map(|v| [v.y0, v.y1]).max().unwrap() + 1;
    let mut counts = Grid::fill(w, h, |_, _| 0usize);
    for Vent { x0, y0, x1, y1 } in vents {
        let mut x = x0;
        let mut y = y0;
        loop {
            counts[(x, y)] += 1;
            if x == x1 && y == y1 {
                break;
            }
            match x0.cmp(&x1) {
                Less => x += 1,
                Equal => (),
                Greater => x -= 1,
            }
            match y0.cmp(&y1) {
                Less => y += 1,
                Equal => (),
                Greater => y -= 1,
            }
        }
    }
    counts.iter().filter(|&&c| c > 1).count()
}

fn part1(mut vents: Vec<Vent>) -> usize {
    vents.retain(|v| v.x0 == v.x1 || v.y0 == v.y1);
    solve(vents)
}

fn part2(vents: Vec<Vent>) -> usize {
    solve(vents)
}

fn main() {
    run(part1);
    run(part2);
}
