use aoc::*;

fn solve(initial: Vec<usize>, time: usize) -> usize {
    let mut fish = [0; 9];
    for age in initial {
        fish[age] += 1;
    }
    for _ in 0..time {
        let mut new_fish = [0; 9];
        for (age, count) in fish.into_iter().enumerate() {
            if age == 0 {
                new_fish[6] += count;
                new_fish[8] += count;
            } else {
                new_fish[age - 1] += count;
            }
        }
        fish = new_fish;
    }
    fish.into_iter().sum()
}

fn part1(fish: Vec<usize>) -> usize {
    solve(fish, 80)
}

fn part2(fish: Vec<usize>) -> usize {
    solve(fish, 256)
}

fn main() {
    run(part1);
    run(part2);
}
