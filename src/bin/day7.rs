use aoc::*;

fn part1(crabs: Vec<usize>) -> isize {
    (0..*crabs.iter().max().unwrap())
        .map(|mid| {
            crabs
                .iter()
                .map(|&x| (x as isize - mid as isize).abs())
                .sum()
        })
        .min()
        .unwrap()
}

fn part2(crabs: Vec<usize>) -> isize {
    (0..*crabs.iter().max().unwrap())
        .map(|mid| {
            crabs
                .iter()
                .map(|&x| {
                    let d = (x as isize - mid as isize).abs();
                    (d + 1) * d / 2
                })
                .sum()
        })
        .min()
        .unwrap()
}

fn main() {
    run(part1);
    run(part2);
}
