use aoc::*;
use itertools::Itertools;

#[derive(Debug, Inpt)]
#[inpt(regex = r"([a-g ]+) \| ([a-g ]+)")]
struct Line {
    signal: Vec<Word<String>>,
    output: Vec<Word<String>>,
}

fn part1(input: Vec<Line>) -> usize {
    input
        .into_iter()
        .flat_map(|l| l.output.clone())
        .filter(|s| [2 /*1*/, 4 /*4*/, 3 /*7*/, 7 /*8*/].contains(&s.inner.len()))
        .count()
}

fn apply_permutation(perm: &[u8], to: &str) -> Option<char> {
    let mut permuted: Vec<u8> = to
        .chars()
        .map(|c| perm[c as usize - 'a' as usize])
        .collect();
    permuted.sort();
    Some(match permuted.as_slice() {
        b"abcefg" => '0',
        b"cf" => '1',
        b"acdeg" => '2',
        b"acdfg" => '3',
        b"bcdf" => '4',
        b"abdfg" => '5',
        b"abdefg" => '6',
        b"acf" => '7',
        b"abcdefg" => '8',
        b"abcdfg" => '9',
        _ => return None,
    })
}

fn solve_permutation(i: &[Word<String>]) -> Vec<u8> {
    for perm in (b'a'..=b'g').permutations(7) {
        let mut digits: Vec<_> = i
            .iter()
            .flat_map(|s| apply_permutation(&perm, &s.inner))
            .collect();
        digits.sort();
        if digits.into_iter().eq('0'..='9') {
            return perm;
        }
    }

    panic!()
}

fn part2(input: Vec<Line>) -> usize {
    input
        .into_iter()
        .map(|line| {
            let permutation = solve_permutation(&*line.signal);
            line.output
                .iter()
                .map(|s| apply_permutation(&permutation, &s.inner).unwrap())
                .collect::<String>()
                .parse::<usize>()
                .unwrap()
        })
        .sum()
}

fn main() {
    run(part1);
    run(part2);
}
