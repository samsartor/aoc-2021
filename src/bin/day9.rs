use aoc::*;

fn part1(i: Grid) -> u32 {
    let mut risk = 0;
    for (x, y) in i.coords() {
        let p = i[(x, y)];
        if i.neighbors(x, y).all(|c| &p < c) {
            risk += p.to_digit(10).unwrap() + 1;
        }
    }
    risk
}

fn part2(i: Grid) -> usize {
    let mut basins = Vec::new();
    for (x, y) in i.coords() {
        let p = i[(x, y)];
        if !i.neighbors(x, y).all(|&c| p < c) {
            continue;
        }

        let mut size = 0;
        let mut marked = i.clone().map(|_, _, _| false);
        let mut stack = vec![(x, y)];
        while let Some((x, y)) = stack.pop() {
            match i.get(x, y) {
                Some('9') | None => continue,
                _ => (),
            };
            if !marked[(x, y)] {
                marked[(x, y)] = true;
                size += 1;
                stack.push((x - 1, y));
                stack.push((x + 1, y));
                stack.push((x, y - 1));
                stack.push((x, y + 1));
            }
        }
        basins.push(size);
    }
    basins.sort();
    basins
        .into_iter()
        .rev()
        .take(3)
        .fold(1, |a: usize, b: usize| a * b)
}

fn main() {
    run(part1);
    run(part2);
}
