use inpt::Inpt;
use std::{convert::TryFrom, fmt};

pub struct BitReader<'b> {
    pub offset: usize,
    pub bits: &'b [bool],
}

impl<'b> BitReader<'b> {
    pub fn new(offset: usize, bits: &'b [bool]) -> Self {
        Self { offset, bits }
    }

    pub fn has(&self) -> bool {
        !self.bits.is_empty()
    }

    pub fn int(&self) -> usize {
        let mut i = 0;
        for &bit in self.bits {
            i <<= 1;
            if bit {
                i |= 1;
            }
        }
        i
    }

    pub fn pop_n(&mut self, n: usize) -> Self {
        let (left, right) = self.bits.split_at(n);
        let old_offset = self.offset;
        self.offset += n;
        self.bits = right;
        BitReader::new(old_offset, left)
    }

    pub fn pop_int(&mut self, n: usize) -> usize {
        self.pop_n(n).int()
    }

    pub fn pop_bool(&mut self) -> bool {
        self.pop_n(1).bits[0]
    }
}

#[derive(Inpt)]
#[inpt(try_from = "&'s str")]
pub struct Hex {
    pub bits: Vec<bool>,
}

impl Hex {
    pub fn read(&self) -> BitReader {
        BitReader::new(0, &self.bits)
    }
}

#[derive(Debug)]
pub struct NotHexError(pub char);
impl fmt::Display for NotHexError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?} is not a hex digit", self.0)
    }
}
impl std::error::Error for NotHexError {}

impl<'s> TryFrom<&'s str> for Hex {
    type Error = NotHexError;

    fn try_from(hex: &'s str) -> Result<Self, Self::Error> {
        let mut bits = Vec::new();
        for char in hex.chars() {
            let hex = char.to_digit(16).ok_or(NotHexError(char))?;
            for i in (0..4).rev() {
                bits.push((hex >> i) & 1 != 0);
            }
        }
        Ok(Hex { bits })
    }
}
