use inpt::split::Group;
use inpt::{CharClass, Inpt, InptIter, InptStep, RecursionGuard, WHITESPACE};
use inpt::{InptError, InptResult, ResultExt};
use std::any::Any;
use std::fmt::{self, Write};
use std::ops::{Index, IndexMut};

#[derive(Clone, Eq, PartialEq)]
pub struct Grid<T = char> {
    pub w: usize,
    pub h: usize,
    pub cells: Vec<T>,
}

impl<'s, T: Inpt<'s>> Grid<T> {
    fn from_lines_impl(lines: impl IntoIterator<Item = &'s str>) -> InptResult<'s, Self> {
        let mut w = None;
        let mut h = 0;
        let mut cells = Vec::new();
        for line in lines {
            let prev_len = cells.len();
            let mut elements = InptIter::new(line, WHITESPACE);
            cells.extend(&mut elements);
            elements.outcome?;
            let line_len = cells.len() - prev_len;

            if *w.get_or_insert(line_len) != line_len {
                return Err(InptError::expected::<Self>(line))
                    .message_inside(format_args!("inconsistent width of {}", line_len))
                    .within(line); // TODO: add message
            }

            h += 1;
        }

        Ok(Grid {
            w: w.unwrap_or(0),
            h,
            cells,
        })
    }

    pub fn from_lines(lines: impl IntoIterator<Item = &'s str>) -> Self {
        Grid::from_lines_impl(lines).unwrap()
    }
}

impl Grid<()> {
    pub fn unit(w: usize, h: usize) -> Self {
        Self {
            w,
            h,
            cells: vec![(); w * h],
        }
    }
}

impl<T> Grid<T> {
    pub const DIRECTIONS: [(isize, isize); 8] = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ];

    pub fn fill(w: usize, h: usize, mut f: impl FnMut(usize, usize) -> T) -> Self {
        Grid::unit(w, h).map(|x, y, ()| f(x, y))
    }

    pub fn map<V>(mut self, mut f: impl FnMut(usize, usize, T) -> V) -> Grid<V> {
        let mut this = self.cells.drain(..);
        let mut cells = Vec::with_capacity(self.w * self.h);
        for y in 0..self.h {
            for x in 0..self.w {
                cells.push(f(x, y, this.next().unwrap()));
            }
        }

        Grid {
            w: self.w,
            h: self.h,
            cells,
        }
    }

    pub fn apply<V>(self, mut f: impl FnMut(usize, usize, &T) -> V) -> Grid<V> {
        let mut this = self.cells.iter();
        let mut cells = Vec::with_capacity(self.w * self.h);
        for y in 0..self.h {
            for x in 0..self.w {
                cells.push(f(x, y, this.next().unwrap()));
            }
        }

        Grid {
            w: self.w,
            h: self.h,
            cells,
        }
    }

    pub fn ati(&self, x: isize, y: isize) -> T
    where
        T: Copy,
    {
        self[(x, y)]
    }

    pub fn atu(&self, x: usize, y: usize) -> T
    where
        T: Copy,
    {
        self[(x, y)]
    }

    pub fn get(&self, x: isize, y: isize) -> Option<&T> {
        if x < 0 || y < 0 || x >= self.w as isize || y >= self.h as isize {
            None
        } else {
            Some(&self[(x as usize, y as usize)])
        }
    }

    pub fn get_mut(&mut self, x: isize, y: isize) -> Option<&mut T> {
        if x < 0 || y < 0 || x >= self.w as isize || y >= self.h as isize {
            None
        } else {
            Some(&mut self[(x as usize, y as usize)])
        }
    }

    pub fn set(&mut self, x: isize, y: isize, value: T) {
        if x >= 0 && y >= 0 && x < self.w as isize && y < self.h as isize {
            self[(x as isize, y as isize)] = value;
        }
    }

    pub fn alter(&mut self, x: isize, y: isize, f: impl FnOnce(&T) -> T) {
        if x >= 0 && y >= 0 && x < self.w as isize && y < self.h as isize {
            self[(x, y)] = f(&self[(x, y)]);
        }
    }

    pub fn neighbors(&self, x: isize, y: isize) -> impl Iterator<Item = &T> + '_ {
        Self::DIRECTIONS
            .iter()
            .filter_map(move |&(dx, dy)| self.get(x + dx, y + dy))
    }

    pub fn coords(&self) -> impl Iterator<Item = (isize, isize)> {
        let h = self.h;
        (0..self.w).flat_map(move |x| (0..h).map(move |y| (x as isize, y as isize)))
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> + '_ {
        self.cells.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> + '_ {
        self.cells.iter_mut()
    }

    pub fn cast(
        &self,
        mut x: isize,
        mut y: isize,
        dx: isize,
        dy: isize,
    ) -> impl Iterator<Item = &T> + '_ {
        std::iter::from_fn(move || {
            let out = self.get(x, y);
            x += dx;
            y += dy;
            out
        })
    }
}

impl<T> Index<(usize, usize)> for Grid<T> {
    type Output = T;

    fn index(&self, (x, y): (usize, usize)) -> &T {
        assert!(x < self.w);
        assert!(y < self.h);
        &self.cells[x + y * self.w]
    }
}

impl<T> IndexMut<(usize, usize)> for Grid<T> {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut T {
        assert!(x < self.w);
        assert!(y < self.h);
        &mut self.cells[x + y * self.w]
    }
}

impl<T> Index<(isize, isize)> for Grid<T> {
    type Output = T;

    fn index(&self, (x, y): (isize, isize)) -> &T {
        &self[(x as usize, y as usize)]
    }
}

impl<T> IndexMut<(isize, isize)> for Grid<T> {
    fn index_mut(&mut self, (x, y): (isize, isize)) -> &mut T {
        &mut self[(x as usize, y as usize)]
    }
}

impl<'s, T: Inpt<'s>> Inpt<'s> for Grid<T> {
    fn step(
        input: &'s str,
        end: bool,
        trimmed: CharClass,
        guard: &mut RecursionGuard,
    ) -> InptStep<'s, Self> {
        let group = Group::<&'s str>::step(input, end, trimmed, guard);
        InptStep {
            rest: group.rest,
            data: group
                .data
                .and_then(|g| Self::from_lines_impl(g.inner.lines())),
        }
    }

    fn default_trim(inherited: CharClass) -> CharClass {
        Group::<&'s str>::default_trim(inherited)
    }
}

impl fmt::Display for Grid<char> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h {
            if y != 0 {
                writeln!(f)?;
            }
            for x in 0..self.w {
                write!(f, "{}", &self[(x, y)])?;
            }
        }

        Ok(())
    }
}

default impl<T: fmt::Display> fmt::Display for Grid<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.h {
            if y != 0 {
                writeln!(f)?;
            }
            for x in 0..self.w {
                if x != 0 {
                    write!(f, " ")?;
                }
                write!(f, "{}", &self[(x, y)])?;
            }
        }

        Ok(())
    }
}

impl<T: fmt::Debug + Any> fmt::Debug for Grid<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut l = f.debug_list();
        for y in 0..self.h {
            let mut entry = String::new();
            for x in 0..self.w {
                let v = &self[(x, y)];
                if let Some(c) = (v as &dyn Any).downcast_ref::<char>() {
                    write!(&mut entry, "{}", c).unwrap();
                } else {
                    if !entry.is_empty() {
                        entry.push(' ');
                    }
                    write!(&mut entry, "{:?}", v).unwrap();
                }
            }
            l.entry(&entry);
        }
        l.finish()
    }
}
