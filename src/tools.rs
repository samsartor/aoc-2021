use fxhash::{FxHashMap, FxHashSet};
use inpt::{inpt, Inpt};
use std::hash::Hash;
use std::iter::repeat;
use std::{collections::*, ops::Index};

pub type I = i64;
pub type V<T> = Vec<T>;
pub type S<T> = FxHashSet<T>;
pub type M<K, V> = FxHashMap<K, V>;

pub use std::collections::hash_map::Entry;
pub use std::mem::{replace, swap};

pub trait StrExt {
    fn read<'s, T: Inpt<'s>>(&'s self) -> T;

    fn at(&self, idx: usize) -> char;
}

impl StrExt for str {
    fn read<'s, T: Inpt<'s>>(&'s self) -> T {
        inpt(self).unwrap()
    }

    fn at(&self, idx: usize) -> char {
        self[idx..].chars().next().expect("indexed end of string")
    }
}

pub trait UintExt {
    fn set_bit(self, idx: usize, value: bool) -> Self;
    fn get_bit(self, idx: usize) -> bool;
}

impl UintExt for usize {
    fn set_bit(self, idx: usize, value: bool) -> Self {
        let shifted = 1 << idx;
        match value {
            true => self | shifted,
            false => self & !shifted,
        }
    }

    fn get_bit(self, idx: usize) -> bool {
        (self >> idx) & 1 != 0
    }
}

impl UintExt for u64 {
    fn set_bit(self, idx: usize, value: bool) -> Self {
        let shifted = 1 << idx;
        match value {
            true => self | shifted,
            false => self & !shifted,
        }
    }

    fn get_bit(self, idx: usize) -> bool {
        (self >> idx) & 1 != 0
    }
}

pub trait MapExt<K, V> {
    fn mutate(&mut self, key: K) -> &mut V;
}

impl<K: Hash + Eq, V: Default> MapExt<K, V> for HashMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

impl<K: Ord, V: Default> MapExt<K, V> for BTreeMap<K, V> {
    fn mutate(&mut self, key: K) -> &mut V {
        self.entry(key).or_default()
    }
}

pub trait IterExt<I>: Sized {
    fn count_eq(self, value: I) -> usize
    where
        I: PartialEq;

    fn reduce_all<V>(self, start: V, reduce: impl FnMut(&V, &I) -> V) -> FxHashMap<I, V>
    where
        I: Hash + Eq;

    fn count_all(self) -> FxHashMap<I, usize>
    where
        I: Hash + Eq,
    {
        self.reduce_all(0, |n, _| *n + 1)
    }
}

impl<T: IntoIterator> IterExt<T::Item> for T {
    fn count_eq(self, value: T::Item) -> usize
    where
        T::Item: PartialEq,
    {
        self.into_iter().filter(eq(value)).count()
    }

    fn reduce_all<V>(
        self,
        start: V,
        mut reduce: impl FnMut(&V, &T::Item) -> V,
    ) -> FxHashMap<T::Item, V>
    where
        T::Item: Hash + Eq,
    {
        let mut map = FxHashMap::default();
        for i in self {
            let e = map.entry(i);
            match e {
                Entry::Occupied(mut e) => {
                    *e.get_mut() = reduce(e.get(), e.key());
                }
                Entry::Vacant(e) => {
                    let v = reduce(&start, e.key());
                    e.insert(v);
                }
            }
        }
        map
    }
}

pub trait SliceExt<'s, T> {
    fn combs<const K: usize>(self) -> Combs<'s, T, [usize; K]>;

    fn combs_with_k(self, k: usize) -> Combs<'s, T, Vec<usize>>;

    fn replaced_combs<const K: usize>(self) -> RCombsConst<'s, T, K>;

    fn replaced_combs_with_k(self, k: usize) -> RCombs<'s, T>;
}

impl<'s, T> SliceExt<'s, T> for &'s [T] {
    /// Iterate over all combitations of `K` elemnts of self, *without
    /// replacement*. `K` is a constant.
    fn combs<const K: usize>(self) -> Combs<'s, T, [usize; K]> {
        Combs::new(self.as_ref())
    }

    /// Iterate over all combitations of `K` elemnts of self, *without
    /// replacement*. `K` is a constant.
    fn combs_with_k(self, k: usize) -> Combs<'s, T, Vec<usize>> {
        Combs::new_with_k(self.as_ref(), k)
    }

    /// Iterate over all combitations of `K` elemnts of self, *with
    /// replacement*. `K` is a constant.
    fn replaced_combs<const K: usize>(self) -> RCombsConst<'s, T, K> {
        RCombsConst::new(self.as_ref())
    }

    /// Iterate over all combitations of `k` elemnts of self, *with
    /// replacement*. `k` is an argument.
    fn replaced_combs_with_k(self, k: usize) -> RCombs<'s, T> {
        RCombs::new(self.as_ref(), k)
    }
}

pub struct EnumeratePerms<I> {
    first: bool,
    stack: I,
    inds: I,
    i: usize,
}

impl<const N: usize> EnumeratePerms<[usize; N]> {
    pub fn new() -> Self {
        let mut inds = [0; N];
        for i in 0..N {
            inds[i] = i;
        }

        EnumeratePerms {
            stack: [0; N],
            inds,
            first: true,
            i: 0,
        }
    }
}

impl<I: FromIterator<usize>> EnumeratePerms<I> {
    pub fn new_with_n(n: usize) -> Self {
        EnumeratePerms {
            stack: repeat(0).take(n).collect(),
            inds: (0..n).collect(),
            first: true,
            i: 0,
        }
    }
}

impl<I: AsMut<[usize]>> EnumeratePerms<I> {
    pub fn step(&mut self) -> Option<&I> {
        if replace(&mut self.first, false) {
            return Some(&self.inds);
        }

        let stack = self.stack.as_mut();
        let inds = self.inds.as_mut();
        let n = inds.len();

        loop {
            if self.i >= n {
                return None;
            }

            if stack[self.i] < self.i {
                if self.i % 2 == 0 {
                    inds.swap(0, self.i);
                } else {
                    inds.swap(stack[self.i], self.i);
                }

                stack[self.i] += 1;
                self.i = 0;
                return Some(&self.inds);
            } else {
                stack[self.i] = 0;
                self.i += 1
            }
        }
    }
}

impl<I: AsMut<[usize]> + Clone> Iterator for EnumeratePerms<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.step()?.clone())
    }
}

pub struct EnumerateCombs<I> {
    first: bool,
    n: usize,
    inds: I,
}

impl<const K: usize> EnumerateCombs<[usize; K]> {
    pub fn new(n: usize) -> Self {
        assert!(n >= K);
        let mut inds = [0; K];
        for i in 0..K {
            inds[i] = i;
        }

        EnumerateCombs {
            inds,
            n,
            first: true,
        }
    }
}

impl<I: FromIterator<usize>> EnumerateCombs<I> {
    pub fn new_with_k(n: usize, k: usize) -> Self {
        assert!(n >= k);
        EnumerateCombs {
            inds: (0..k).collect(),
            n,
            first: true,
        }
    }
}

impl<I: AsMut<[usize]>> EnumerateCombs<I> {
    pub fn step(&mut self) -> Option<&I> {
        if replace(&mut self.first, false) {
            return Some(&self.inds);
        }

        let inds = self.inds.as_mut();
        let k = inds.len();

        let mut to_inc = None;
        for i in (0..k).rev() {
            if inds[i] < self.n - k + i {
                to_inc = Some(i);
                break;
            }
        }

        let to_inc = to_inc?;
        let x = inds[to_inc];
        for i in to_inc..k {
            inds[i] = x - to_inc + i + 1;
        }

        Some(&self.inds)
    }
}

impl<I: AsMut<[usize]> + Clone> Iterator for EnumerateCombs<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.step()?.clone())
    }
}

#[test]
fn test_combs() {
    assert_eq!(
        EnumerateCombs::<[usize; 3]>::new(5).collect::<Vec<_>>(),
        vec![
            [0, 1, 2],
            [0, 1, 3],
            [0, 1, 4],
            [0, 2, 3],
            [0, 2, 4],
            [0, 3, 4],
            [1, 2, 3],
            [1, 2, 4],
            [1, 3, 4],
            [2, 3, 4],
        ]
    )
}

pub struct Combs<'l, T, I> {
    list: &'l [T],
    inds: EnumerateCombs<I>,
}

impl<'l, T, const K: usize> Combs<'l, T, [usize; K]> {
    pub fn new(list: &'l [T]) -> Self {
        Combs {
            list,
            inds: EnumerateCombs::new(list.len()),
        }
    }
}

impl<'l, T> Combs<'l, T, Vec<usize>> {
    pub fn new_with_k(list: &'l [T], k: usize) -> Self {
        Combs {
            list,
            inds: EnumerateCombs::new_with_k(list.len(), k),
        }
    }
}

impl<'l, T, const N: usize> Iterator for Combs<'l, T, [usize; N]> {
    type Item = [&'l T; N];

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.inds.next()?.map(|i| &self.list[i]))
    }
}

impl<'l, T> Iterator for Combs<'l, T, Vec<usize>> {
    type Item = Vec<&'l T>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.inds.step()?.iter().map(|&i| &self.list[i]).collect())
    }
}

#[derive(Clone)]
pub struct RCombs<'l, T> {
    list: &'l [T],
    k: usize,
    step: usize,
}

impl<'l, T> RCombs<'l, T> {
    pub fn new(list: &'l [T], k: usize) -> Self {
        Self {
            list,
            k,
            step: list.len().pow(k as u32),
        }
    }
}

impl<'l, T> Iterator for RCombs<'l, T> {
    type Item = RCombList<'l, T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.step == 0 {
            return None;
        }
        self.step -= 1;

        let out = RCombList {
            list: &self.list,
            k: self.k,
            step: self.step,
        };

        Some(out)
    }
}

#[derive(Clone)]
pub struct RCombList<'l, T> {
    list: &'l [T],
    k: usize,
    step: usize,
}

impl<'l, T> Index<usize> for RCombList<'l, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        assert!(index < self.k);
        let i = (self.step / self.list.len().pow(index as u32)) % self.list.len();
        &self.list[i]
    }
}

impl<'l, T> Iterator for RCombList<'l, T> {
    type Item = &'l T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.k == 0 {
            return None;
        }

        let out = &self.list[self.step % self.list.len()];
        self.k -= 1;
        self.step /= self.list.len();

        Some(out)
    }
}

pub struct RCombsConst<'l, T, const K: usize> {
    combs: RCombs<'l, T>,
}

impl<'l, T, const K: usize> RCombsConst<'l, T, K> {
    pub fn new(list: &'l [T]) -> Self {
        RCombsConst {
            combs: RCombs::new(list, K),
        }
    }
}

impl<'l, T, const K: usize> Iterator for RCombsConst<'l, T, K> {
    type Item = [&'l T; K];

    fn next(&mut self) -> Option<Self::Item> {
        let mut list = self.combs.next()?;
        let arr = [(); K];
        Some(arr.map(|()| list.next().unwrap()))
    }
}

pub fn eq<T: PartialEq>(value: T) -> impl Fn(&T) -> bool {
    move |x| value.eq(x)
}
